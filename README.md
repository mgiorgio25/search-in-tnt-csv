# Search in TNT CSV
Questa è una applicazione web che permette di effettuare ricerche all'interno del file CSV di TNT Village.

L'applicazione è stata realizzata in Python utilizzando il framework Django. 

Per poter avviare l'applicazione si deve eseguire dall'interno della cartella "SearchInTNTCsv" il comando 
```shell script
python manage.py runserver
```