from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render, redirect

from .models import SearchInCSV
# Create your views here.

magnetFirstPart = "magnet:/?xt=urn:btih:"
magnetSecondPart = "&dn="
magnetThirdPart = "&tr=http%3A%2F%2Ftracker.tntvillage.scambioetico.org%3A2710%2Fannounce" \
                  "&tr=udp%3A%2F%2Ftracker.tntvillage.scambioetico.org%3A2710%2Fannounce" \
                  "&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969%2Fannounce" \
                  "&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969%2Fannounce" \
                  "&tr=udp%3A%2F%2FIPv6.leechers-paradise.org%3A6969%2Fannounce" \
                  "&tr=udp%3A%2F%2Ftracker.internetwarriors.net%3A1337%2Fannounce" \
                  "&tr=udp%3A%2F%2Ftracker.tiny-vps.com%3A6969%2Fannounce" \
                  "&tr=udp%3A%2F%2Ftracker.mg64.net%3A2710%2Fannounce" \
                  "&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80%2Fannounce"


def index(request):
    result = list()
    template = loader.get_template('index.html')
    return render(request, 'index.html', {'result': result})


def search(request):
    if 'param' not in request.POST:
        return redirect('index')

    result = SearchInCSV().search(param=request.POST['param'])
    template = loader.get_template('index.html')
    return render(request, 'index.html', {'searchParam': request.POST['param'], 'result': result,
                                          'magnetFirstPart': magnetFirstPart, 'magnetSecondPart': magnetSecondPart,
                                          'magnetThirdPart': magnetThirdPart})