from django.db import models
import csv
import re

# Create your models here.


class SearchInCSV:
    def search(self, param =""):
        # read csv, and split on "," the line
        csv_file = csv.reader(open('dump_release_tntvillage_2019-08-30.csv', "r", encoding="utf8"), delimiter=",")

        result = list()
        param = param.replace(" ", "(.*)")
        # loop through csv list
        for row in csv_file:
            # if current rows 2nd value is equal to input, print that row
            if re.search(param, row[5], re.IGNORECASE):
                result.append(row)
        return result