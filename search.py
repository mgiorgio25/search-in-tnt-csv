import csv
import re

#String to search
parm = "extended"

#read csv, and split on "," the line
csv_file = csv.reader(open('dump_release_tntvillage_2019-08-30.csv', "r", encoding="utf8"), delimiter=",")

result = list()
#loop through csv list
for row in csv_file:
    #if current rows 2nd value is equal to input, print that row
    if re.search(parm, row[5], re.IGNORECASE):
        result.append(row)
        print(row)